/**
 * @file
 * Select2 integration
 *
 */
(function ($) {

  Drupal.behaviors.multiselect = {
    attach: function (context, settings) {
      $('[data-multi-select-widget]', context).once().each(function () {
        $(this).multiSelect();
      });

      $('[data-multi-select-widget-search]', context).once().each(function () {
        $(this).multiSelect({
          selectableHeader: Drupal.theme.multiselect_search.selectable,
          selectionHeader: Drupal.theme.multiselect_search.selection,
          afterInit: function (ms) {
            var that = this,
              $selectableSearch = that.$selectableUl.prev(),
              $selectionSearch = that.$selectionUl.prev();

            $('#' + that.$container.attr('id') + ' .ms-selectable .ms-list').searcher({
              itemSelector:  'li',
              textSelector:  "",
              inputSelector: $selectableSearch
            });

            $('#' + that.$container.attr('id') + ' .ms-selection .ms-list').searcher({
              itemSelector:  'li',
              textSelector:  "",
              inputSelector: $selectionSearch
            });
          }
        })
      });
    }
  };

  Drupal.theme.multiselect_search = {} || Drupal.theme.multiselect_search;
  Drupal.theme.multiselect_search.selectable = function () {
    return '<label>Selectable</label><input class="ms-search-box form-text form-item search" placeholder="' + Drupal.t('Search') + '" />'
  };

  Drupal.theme.multiselect_search.selection = function () {
    return '<label>Selected</label><input class="ms-search-box form-text form-item search" placeholder="' + Drupal.t('Search') + '" />'
  };

})(jQuery);
