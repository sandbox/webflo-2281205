<?php

/**
 * @file
 * Contains \Drupal\select2\Entity\FieldWidget\AutocompleteWidget.
 */

namespace Drupal\multiselect\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\options\Plugin\Field\FieldWidget\SelectWidget;
use Drupal\user\EntityOwnerInterface;

/**
 * Plugin implementation of the 'entity_reference autocomplete-tags' widget.
 *
 * @FieldWidget(
 *   id = "multiselect_select",
 *   label = @Translation("Mutliselect Widget"),
 *   description = @Translation("An autocomplete text field."),
 *   field_types = {
 *     "entity_reference",
 *     "list_integer",
 *     "list_float",
 *     "list_text"
 *   },
 *   multiple_values = TRUE
 * )
 */
class MultiSelectWidget extends SelectWidget {

  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, array &$form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['#attributes']['data-multi-select-widget-search'] = TRUE;
    $element['#attached']['library'][] = 'multiselect/multiselect';
    $element['#attached']['js'][] = drupal_get_path('module', 'multiselect') . '/js/multiselect.js';

    return $element;
  }

}
